import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:math' as math;
import 'res/colors.dart';
import 'dart:ui';

class MainLayout extends StatelessWidget {
  final PreferredSizeWidget appBar;
  final Widget child;
  final bool Function() willPopScope;
  final FloatingActionButton floatingActionButton;

  MainLayout({
    this.appBar,
    this.child,
    this.willPopScope,
    this.floatingActionButton,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (willPopScope != null) {
          return willPopScope();
        }
        return false;
      },
      child: Scaffold(
        appBar: appBar,
        body: child,
        bottomNavigationBar: Container(
          decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(
            color: black.withOpacity(0.4),
            width: 1.0,
          ))),
          height: 75.0.w,
          width: double.infinity,
          child: ClipRRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 5.0,
                sigmaY: 5.0,
              ),
              child: BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  backgroundColor: Color(pureWhite).withOpacity(0.85),
                  selectedItemColor: black.withOpacity(0.4),
                  unselectedItemColor: black.withOpacity(0.4),
                  unselectedLabelStyle:
                      TextStyle(fontSize: ScreenUtil().setSp(10.0)),
                  selectedLabelStyle:
                      TextStyle(fontSize: ScreenUtil().setSp(10.0)),
                  items: <BottomNavigationBarItem>[
                    item(
                      icon: Icon(
                        IconData(0xF2F5,
                            fontFamily: CupertinoIcons.iconFont,
                            fontPackage: CupertinoIcons.iconFontPackage),
                        size: 25.0.w,
                      ),
                      title: Text('Search'),
                    ),
                    item(
                      icon: SvgPicture.asset(
                        'assets/arcade.svg',
                        height: 25.w,
                        width: 25.w,
                        color: black.withOpacity(0.4),
                      ),
                      title: Text('Arcade'),
                    ),
                    item(
                      icon: Image.asset(
                        'assets/apps.png',
                        height: 25.w,
                        width: 25.w,
                        color: black.withOpacity(0.4),
                      ),
                      title: Text('Apps'),
                    ),
                    item(
                      icon: Image.asset(
                        'assets/rocket.png',
                        height: 25.w,
                        width: 25.w,
                        color: black.withOpacity(0.4),
                      ),
                      title: Text('Games'),
                    ),
                    item(
                      icon: Transform(
                          alignment: Alignment.center,
                          transform: Matrix4.rotationY(math.pi),
                          child: Image.asset(
                            'assets/today.png',
                            height: 25.w,
                            width: 25.w,
                            color: black.withOpacity(0.4),
                          )),
                      title: Text('Today'),
                    ),
                  ]),
            ),
          ),
        ),
        floatingActionButton: floatingActionButton,
        extendBody: true,
      ),
    );
  }
}

BottomNavigationBarItem item({Widget icon, Text title}) {
  return BottomNavigationBarItem(
    icon: icon,
    title: Padding(padding: EdgeInsets.only(bottom: 24.0.h), child: title),
  );
}
