import 'package:flutter/material.dart';


import 'main_page/home_page.dart';
import 'res/colors.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'AppStore',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        backgroundColor: Color(pureWhite),
      ),
      home: MyHomePage(),
      builder: (ctx, child) {
        return child;
      },
    );
  }
}

