import 'package:flutter/material.dart';

const int pureWhite = 0xFFffffff;
const int lightGrey = 0xFF929093;
const int lightDarkGrey = 0xFFdddadd;
const int blueLink = 0xFF0080ff;
const Color black = Color(0xFF000000);