import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'colors.dart';

TextStyle boldStyle(double size) {
  return TextStyle(fontSize: size, fontWeight: FontWeight.bold);
}

final TextStyle linkStyle = TextStyle(
  color: Color(blueLink),
  fontWeight: FontWeight.w500,
  fontSize: ScreenUtil().setSp(13.66),
);

final TextStyle mainTextStyle = TextStyle(
  fontSize: ScreenUtil().setSp(17.3),
  color: Color(lightGrey),
  height: 1.3,
  fontWeight: FontWeight.w300,
);