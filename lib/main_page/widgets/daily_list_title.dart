import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_building/res/colors.dart';
import 'package:layout_building/res/text_styles.dart';

class DailyListTitle extends StatelessWidget {
  final double _titleSize = 25;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 43.0.h, right: 30.0.w),
      child: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerRight,
            child: Text(
              'THE DAILY LIST',
              style: TextStyle(color: Color(lightGrey), fontSize: ScreenUtil().setSp(13.6), fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(height: 8.0.h),
          Align(
            alignment: Alignment.centerRight,
            child: Text(
              'Shop With',
              style: boldStyle(ScreenUtil().setSp(_titleSize)),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Text(
              'Curbside Pickup',
              style: boldStyle(ScreenUtil().setSp(_titleSize)),
            ),
          ),
        ],
      ),
    );
  }
}
