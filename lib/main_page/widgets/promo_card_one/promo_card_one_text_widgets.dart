import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_building/res/colors.dart';

class PromoCard1 {
  Widget topWidget() {
    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.centerRight,
          child: Text(
            'MEET THE CREATIVE',
            style: TextStyle(
              color: Color(pureWhite).withOpacity(0.8),
              fontSize: ScreenUtil().setSp(13.5),
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(height: 6.0.h),
        Align(
          alignment: Alignment.centerRight,
          child: Text(
            'He\'s With the Band',
            style: TextStyle(
              fontSize: ScreenUtil().setSp(24.0),
              fontWeight: FontWeight.bold,
              color: Color(pureWhite),
            ),
          ),
        ),
      ],
    );
  }

  Widget bottomWidget() {
    return Padding(
      padding: EdgeInsets.only(left: 5.0.w, top: 310.0.w),
      child: Align(
        alignment: Alignment.bottomLeft,
        child: Text(
          'Level up with concert photographer Greg Noire\'s Lightroom presets.',
          style: TextStyle(
              color: Color(pureWhite),
              fontSize: ScreenUtil().setSp(13.5),
              fontWeight: FontWeight.w400,
              height: 1.3),
        ),
      ),
    );
  }
}
