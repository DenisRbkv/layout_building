import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_building/res/colors.dart';


class TopDate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: EdgeInsets.only(top: 70.0.h, right: 20.0.w),
        child: Text(
          DateFormat.MMMMEEEEd().format(DateTime.now()).toUpperCase(),
          style: TextStyle(color: Color(lightGrey), fontSize: ScreenUtil().setSp(11.5)),
        ),
      ),
    );
  }
}
