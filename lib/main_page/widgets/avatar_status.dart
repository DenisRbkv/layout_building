import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_building/res/images.dart';
import 'package:layout_building/res/text_styles.dart';

class AvatarStatus extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0.w, vertical: 5.0.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          CircleAvatar(
            backgroundImage: NetworkImage(userAvatar),
            backgroundColor: Colors.transparent,
            radius: 16.0,
          ),
          Text(
            'Today',
            style: boldStyle(ScreenUtil().setSp(30.0)),
          ),
        ],
      ),
    );
  }
}
