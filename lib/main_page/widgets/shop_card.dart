import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_building/res/widget_styles.dart';
import 'package:layout_building/res/colors.dart';
import 'package:layout_building/res/images.dart';
import 'shop_card_one/shop_card_row.dart';

class ShopCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 255.0.w,
      width: double.infinity,
      margin: EdgeInsets.symmetric(vertical: 15.0.h, horizontal: 15.0.w),
      decoration: BoxDecoration(
        color: Color(pureWhite),
        borderRadius: bordersCircular,
        boxShadow: [
          BoxShadow(
            color: Colors.black26.withOpacity(0.04),
            blurRadius: 6.0,
            spreadRadius: 0.0,
            offset: Offset(
              0.0, // horizontal
              18.0, // vertical
            ),
          ),
        ],
      ),
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 30.0.w),
              child: Column(
                children: <Widget>[
                  ShopCardRow(
                    title: 'Allset: Food Pickup\n& Takeout',
                    notation: 'Order ahead at restaurants',
                    icon: icon1,
                  ),
                  ShopCardRow(
                    title: 'Walmart - shopping\n& grocery',
                    notation: 'Shop the whole store and',
                    icon: icon2,
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                              left: 15.0.w, top: 18.0.h, bottom: 19.0.h),
                          child: Icon(
                            IconData(0xf407,
                                fontFamily: CupertinoIcons.iconFont,
                                fontPackage: CupertinoIcons.iconFontPackage),
                            color: Color(blueLink),
                            size: 27.0,
                          )),
                      SizedBox(width: 30.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 18.0.w),
                            child: Text(
                              'DoorDash - Food Delivery',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: black,
                                  fontSize: ScreenUtil().setSp(13.7)),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 4.0.h),
                            child: Text(
                              'Restaurant Eats & Drinks\n To Go',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Color(lightGrey),
                                  fontSize: ScreenUtil().setSp(11.85),
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: 13.0.w),
                      Container(
                        height: 45.0.h,
                        width: 45.0.w,
                        child: ClipRRect(
                          borderRadius: bordersCircular,
                          child: FittedBox(
                            fit: BoxFit.fill,
                            child: Image.network(icon3),
                          ),
                        ),
                      ),
                    ],
                  ),
                  ShopCardRow(
                    title: 'Starbucks',
                    notation: 'Order, pay, and earn Stars',
                    icon: icon4,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
