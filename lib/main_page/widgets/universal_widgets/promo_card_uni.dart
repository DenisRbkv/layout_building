import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/foundation.dart';

import 'package:layout_building/res/widget_styles.dart';
import 'package:layout_building/detail_page/detail_page.dart';
import 'package:layout_building/animations/promo_card_hero_animation.dart';

class PromoCard extends StatelessWidget {
  final String cardImage;
  final Widget topWidget;
  final Widget bottomWidget;

  PromoCard({
    @required this.cardImage,
    @required this.topWidget,
    @required this.bottomWidget,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).push(
        PageRouteBuilder(
          opaque: false,
          transitionDuration: Duration(milliseconds: 700),
          fullscreenDialog: true,
          pageBuilder: (context, _, __) => DetailPage(
            cardImage: cardImage,
            topWidget: topWidget,
            bottomWidget: bottomWidget,
          ),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0.w, vertical: 2.0.h),
        child: PromoCardHeroAnimation(
          child: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 415.0.w,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26.withOpacity(0.07),
                      blurRadius: 4.0,
                      spreadRadius: -2.0,
                      offset: Offset(
                        1.0, // horizontal
                        15.0, // vertical
                      ),
                    ),
                  ],
                ),
                child: ClipRRect(
                  borderRadius: bordersCircular,
                  child: FittedBox(
                    fit: BoxFit.fill,
                    child: Image.network(cardImage),
                  ),
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: 15.0.w, vertical: 15.0.h),
                child: Column(
                  children: <Widget>[
                    topWidget,
                    bottomWidget,
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
