import 'package:flutter/material.dart';
import 'package:layout_building/res/text_styles.dart';

class LinkTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, top: 20.0, bottom: 30.0),
      child: Text(
        'GET',
        style: linkStyle,
      ),
    );
  }
}
