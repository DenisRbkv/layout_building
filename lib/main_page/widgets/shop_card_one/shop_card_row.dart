import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_building/main_page/widgets/shop_card_one/link_text_widget.dart';
import 'package:layout_building/res/widget_styles.dart';
import 'package:layout_building/res/colors.dart';

class ShopCardRow extends StatelessWidget {
  final String icon;
  final String title;
  final String notation;

  ShopCardRow({@required this.icon, @required this.title, @required this.notation});


  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        LinkTextWidget(),
        SizedBox(width: 62.0.w),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 18.0.w),
              child: Text(
                title,
                textAlign: TextAlign.right,
                style: TextStyle(color: black, fontSize: ScreenUtil().setSp(13.7)),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 4.0.h),
              child: Text(
                notation,
                style: TextStyle(color: Color(lightGrey), fontSize: ScreenUtil().setSp(11.85), fontWeight: FontWeight.w300),
              ),
            ),
          ],
        ),
        SizedBox(width: 13.0.w),
        Container(
          height: 45.0.h,
          width: 45.0.w,

          child: ClipRRect(
            borderRadius: bordersCircular,
            child: FittedBox(
              fit: BoxFit.fill,
              child: Image.network(icon),
            ),
          ),
        ),
      ],
    );
  }
}
