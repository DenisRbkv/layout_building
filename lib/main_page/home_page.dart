//PACKAGE
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_building/main_page/widgets/daily_list_title.dart';

//PAGE
//head
import './widgets/top_date.dart';
import './widgets/avatar_status.dart';

//card1
import 'package:layout_building/main_page/widgets/promo_card_one/promo_card_one_text_widgets.dart';

//daily list
import './widgets/shop_card.dart';

//universal
import 'package:layout_building/main_page/widgets/universal_widgets/promo_card_uni.dart';

//res
import 'package:layout_building/res/images.dart';
import 'package:layout_building/res/colors.dart';

//misc
import 'package:layout_building/main_layout.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375.0,
      height: 890.0,
      allowFontScaling: true,
    );
    return MainLayout(

      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            TopDate(), //Dynamic
            SizedBox(
              //Dynamic
              height: 3.0.h,
            ),
            AvatarStatus(), //Dynamic
            PromoCard(
              //Dynamic
              cardImage: card1,
              topWidget: PromoCard1().topWidget(),
              bottomWidget: PromoCard1().bottomWidget(),
            ),
            DailyListTitle(), //Dynamic
            ShopCard(), //Dynamic
            SizedBox(
              height: 500.0,
            ),
          ],
        ),
      ),
    );
  }
}
