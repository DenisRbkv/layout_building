import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class PromoCardHeroAnimation extends StatefulWidget {
  final Widget child;

  PromoCardHeroAnimation({@required this.child});

  @override
  _PromoCardHeroAnimationState createState() => _PromoCardHeroAnimationState();
}

class _PromoCardHeroAnimationState extends State<PromoCardHeroAnimation> {
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'promo_card',
      child: Material(
        child: widget.child,
      ),
      flightShuttleBuilder:
          (flightContext, animation, direction, context, toContext) {
        final Hero toHero = toContext.widget;

        return direction == HeroFlightDirection.push
            ? ScaleTransition(
                scale: animation.drive(
                  Tween<double>(
                    begin: 0.75,
                    end: 1.02,
                  ).chain(
                    CurveTween(
                        curve: Interval(0.4, 1.0, curve: Curves.easeInOut)),
                  ),
                ),
                child: toHero.child,
              )
            : SizeTransition(
                sizeFactor: animation,
                child: toHero.child,
              );
      },
    );
  }
}
