import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/foundation.dart';

import 'package:layout_building/res/text_content.dart';
import 'package:layout_building/res/colors.dart';
import 'package:layout_building/animations/promo_card_hero_animation.dart';

import '../res/text_styles.dart';
import '../res/text_styles.dart';

class DetailPage extends StatefulWidget {
  final String cardImage;
  final Widget topWidget;
  final Widget bottomWidget;

  DetailPage({
    @required this.cardImage,
    @required this.topWidget,
    @required this.bottomWidget,
  });

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> with TickerProviderStateMixin {
  AnimationController heightController;
  AnimationController widthController;

  @override
  void initState() {
    heightController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 700));
    widthController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 700));
    super.initState();
    heightController.forward();
    widthController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                PromoCardHeroAnimation(
                  child: Center(
                    child: SingleChildScrollView(
                      physics: NeverScrollableScrollPhysics(),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: 500.0.w,
                            width: double.infinity,
                            child: FittedBox(
                              fit: BoxFit.fill,
                              child: Image.network(widget.cardImage),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 40.0.w,
                              right: 20.0.w,
                            ),
                            child: widget.topWidget,
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              top: 135.0.w,
                              left: 50.0.w,
                              right: 30.0.w,
                            ),
                            child: widget.bottomWidget,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizeTransition(
                  axis: Axis.horizontal,
                  sizeFactor: Tween<double>(begin: 0.5, end: 1).animate(
                    CurvedAnimation(
                        curve: Curves.easeInOut, parent: widthController),
                  ),
                  child: SizeTransition(
                    sizeFactor: Tween<double>(begin: 0, end: 1).animate(
                      CurvedAnimation(
                          curve: Curves.easeInOut, parent: heightController),
                    ),
                    child: Container(
                      padding: EdgeInsets.only(
                          top: 35.0.h, left: 20.0.w, right: 20.0.w),
                      width: double.infinity,
                      child: Column(
                        children: <Widget>[
                          RichText(
                            text: TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                  text: DUMMY_PARAGRAPH_START,
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(17.3),
                                    color: black,
                                    fontWeight: FontWeight.bold,
                                    height: 1.3,
                                  ),
                                ),
                                TextSpan(
                                  text: DUMMY_TEXT_SHORT,
                                  style: mainTextStyle,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 25.0.h),
                          Text(
                            DUMMY_TEXT,
                            style: mainTextStyle,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 500.0.w),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20.0.w, top: 25.0.w),
            child: InkWell(
                onTap: () {
                  widthController.reverse();
                  heightController.reverse();
                  Navigator.pop(context);
                },
                child: Icon(
                  CupertinoIcons.clear_thick_circled,
                  color: Colors.white,
                  size: 30,
                )),
          ),
        ],
      ),
    );
  }
}
